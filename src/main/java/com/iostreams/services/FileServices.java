package com.iostreams.services;

import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

import java.io.*;

public class FileServices {
    private FileWriterWrapper writerWrapper;
    private BufferedReaderWrapper readerWrapper;
    public File fileStart;

    public FileServices(BufferedReaderWrapper readerWrapper, FileWriterWrapper writerWrapper) {
        this.readerWrapper = readerWrapper;
        this.writerWrapper = writerWrapper;
    }

    public String readFiles(String[] fileNames) {
        int arrayLength = fileNames.length;
        if (arrayLength < 1 || arrayLength > Byte.MAX_VALUE) {
            return null;
        }
        File[] files = new File[arrayLength];
        for (int i = 0; i < fileNames.length; i++) {
            files[i] = new File(fileNames[i]);
        }
        String allText = "";
        for (File file : files) {
            String fileText = readFile(file);
            allText += fileText;
        }
        return allText;
    }

    public String readFile(File file) {
        String fileText = "";

        try(BufferedReader reader = readerWrapper.getBufferedReader(file)) {
            String fileString = reader.readLine();

            while (fileString != null) {
                fileText += fileString;
                fileText += "\n";
                fileString = reader.readLine();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return fileText;
    }

    public void writeFileWithDelete(String text, String fileName) {
        File file = new File(fileName);
        try (FileWriter writer = writerWrapper.getFileWriter(file, false)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFileToEnd(String text, String fileName) {
        File file = new File(fileName);
        try (FileWriter writer = writerWrapper.getFileWriter(file, true)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  void writeFileToStart(String text, String fileName) {
        String fileText = "";
        fileStart = new File(fileName);
        try(BufferedReader reader = readerWrapper.getBufferedReader(fileStart)) {
            String fileString = reader.readLine();

            while (fileString != null) {
                fileText += fileString;
                fileText += "\n";
                fileString = reader.readLine();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        text += fileText;
        try (FileWriter writer = writerWrapper.getFileWriter(fileStart, false)) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
