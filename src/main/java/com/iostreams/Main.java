package com.iostreams;

import com.iostreams.services.FileServices;
import static com.iostreams.utils.FileStorage.*;
import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        String oldFile1Text = "Text in old file1";
        String oldFile2Text = "Text in old file2";
        String oldFile3Text = "Text in old file3";
        String newFile1Text = "Text in new file1";
        String newFile2Text = "Text in new file2";
        String newFile3Text = "Text in new file3";

        try (FileWriter oldFile1Writer = new FileWriter(OLD_FILE_1);
             FileWriter oldFile2Writer = new FileWriter(OLD_FILE_2);
             FileWriter oldFile3Writer = new FileWriter(OLD_FILE_3);
             FileWriter newFile1Writer = new FileWriter(NEW_FILE_1);
             FileWriter newFile2Writer = new FileWriter(NEW_FILE_2);
             FileWriter newFile3Writer = new FileWriter(NEW_FILE_3)) {
            oldFile1Writer.write(oldFile1Text);
            oldFile2Writer.write(oldFile2Text);
            oldFile3Writer.write(oldFile3Text);
            newFile1Writer.write(newFile1Text);
            newFile2Writer.write(newFile2Text);
            newFile3Writer.write(newFile3Text);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReaderWrapper bufferedReaderWrapper = new BufferedReaderWrapper();
        FileWriterWrapper fileWriterWrapper = new FileWriterWrapper();

        FileServices fileServices = new FileServices(bufferedReaderWrapper, fileWriterWrapper);
        String[] fileNames = {"DataStore\\old_file_1.txt", "DataStore\\old_file_2.txt", "DataStore\\old_file_3.txt"};
        String allText = fileServices.readFiles(fileNames);

        fileServices.writeFileWithDelete(allText, "DataStore\\new_file_1.txt");
        fileServices.writeFileToStart(allText, "DataStore\\new_file_2.txt");
        fileServices.writeFileToEnd(allText, "DataStore\\new_file_3.txt");
    }
}
