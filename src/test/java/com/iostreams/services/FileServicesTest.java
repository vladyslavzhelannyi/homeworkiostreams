package com.iostreams.services;

import static com.iostreams.testData.FileServicesTestData.*;
import static org.mockito.Mockito.*;

import com.iostreams.wrappers.BufferedReaderWrapper;
import com.iostreams.wrappers.FileWriterWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.*;

public class FileServicesTest {
    File file = Mockito.mock(File.class);
    BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
    FileWriter fileWriter1 = new FileWriter(new File(TEST_FILE_NAME), false);
    FileWriter writerSpy1 = spy(fileWriter1);
    FileWriter fileWriter2 = new FileWriter(new File(TEST_FILE_NAME), true);
    FileWriter writerSpy2 = spy(fileWriter2);
    BufferedReaderWrapper readerWrapper = Mockito.mock(BufferedReaderWrapper.class);
    FileWriterWrapper writerWrapper = Mockito.mock(FileWriterWrapper.class);
    FileServices cut = new FileServices(readerWrapper, writerWrapper);
    FileServices cutSpy = Mockito.spy(cut);

    public FileServicesTest() throws IOException {
    }

    static Arguments[] readFileTestArgs() {
        return new Arguments[] {
            Arguments.arguments(FILE_TEST_LINE_1, null, null, READ_FILE_RESULT_1),
            Arguments.arguments(FILE_TEST_LINE_1, FILE_TEST_LINE_2, null, READ_FILE_RESULT_2)
        };
    }

    @ParameterizedTest
    @MethodSource("readFileTestArgs")
    void readFileTest(String line1, String line2, String line3, String expected) throws IOException {
        Mockito.when(readerWrapper.getBufferedReader(file)).thenReturn(bufferedReader);
        Mockito.when(bufferedReader.readLine()).thenReturn(line1).thenReturn(line2).thenReturn(line3);
        String actual = cut.readFile(file);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] writeFileWithDeleteFileTestArgs() {
        return new Arguments[] {
            Arguments.arguments(FILE_TEST_LINE_4, TEST_FILE_NAME, false),
            Arguments.arguments(FILE_TEST_LINE_5, TEST_FILE_NAME, false)
        };
    }

    @ParameterizedTest
    @MethodSource("writeFileWithDeleteFileTestArgs")
    void writeFileWithDeleteFileTest(String text, String fileName, boolean ifAddToFile)
            throws IOException {
        Mockito.when(writerWrapper.getFileWriter(new File(fileName), ifAddToFile)).thenReturn(writerSpy1);
        doNothing().when(writerSpy1).write(text);
        cut.writeFileWithDelete(text, fileName);
    }

    static Arguments[] writeFileToEndFileTestArgs() {
        return new Arguments[] {
                Arguments.arguments(FILE_TEST_LINE_4, TEST_FILE_NAME, true),
                Arguments.arguments(FILE_TEST_LINE_5, TEST_FILE_NAME, true)
        };
    }

    @ParameterizedTest
    @MethodSource("writeFileToEndFileTestArgs")
    void writeFileToEndFileTest(String text, String fileName, boolean ifAddToFile)
            throws IOException {
        Mockito.when(writerWrapper.getFileWriter(new File(fileName), ifAddToFile)).thenReturn(writerSpy2);
        doNothing().when(writerSpy2).write(text);
        cut.writeFileToEnd(text, fileName);
    }

    static Arguments[] writeFileToStartFileTestArgs() {
        return new Arguments[] {
                Arguments.arguments(FILE_TEST_LINE_2, FILE_TEST_LINE_4, TEST_FILE_NAME, false),
                Arguments.arguments(FILE_TEST_LINE_3, FILE_TEST_LINE_5, TEST_FILE_NAME, false)
        };
    }

    @ParameterizedTest
    @MethodSource("writeFileToStartFileTestArgs")
    void writeFileToStartFileTest(String stringWrite, String text, String fileName, boolean ifAddToFile) throws IOException {
        Mockito.when(readerWrapper.getBufferedReader(cutSpy.fileStart)).thenReturn(bufferedReader);
        Mockito.when(bufferedReader.readLine()).thenReturn(stringWrite).thenReturn(null);
        Mockito.when(writerWrapper.getFileWriter(new File(fileName), ifAddToFile)).thenReturn(writerSpy1);
        doNothing().when(writerSpy1).write(text);
        cutSpy.writeFileToStart(text, fileName);
    }
}
