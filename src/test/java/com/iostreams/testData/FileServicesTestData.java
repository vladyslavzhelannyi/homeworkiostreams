package com.iostreams.testData;

public class FileServicesTestData {
    public static final String FILE_TEST_LINE_1 = "test line 1";
    public static final String FILE_TEST_LINE_2 = "test line 2";
    public static final String FILE_TEST_LINE_3 = "test line 3";
    public static final String FILE_TEST_LINE_4 = "test line 4";
    public static final String FILE_TEST_LINE_5 = "test line 5";

    public static final String TEST_FILE_NAME = "test_name";

    public static final String READ_FILE_RESULT_1 = FILE_TEST_LINE_1 + "\n";
    public static final String READ_FILE_RESULT_2 = FILE_TEST_LINE_1 + "\n" + FILE_TEST_LINE_2 + "\n";
}
